# Installation

Regardless of whether you want to use it as a standalone application or a module in your project, you can install SamRand via pip as you would any normal python module:

```shell
$ pip install samrand
```

